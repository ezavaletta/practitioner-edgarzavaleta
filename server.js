var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

// Peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users',
          function(request,response)
          {
            console.log('GET' + URL_BASE + 'users');
            response.send(userFile);
          }
        );
// Peticion GET de un 'users' (Instance)
app.get(URL_BASE + 'users/:id',
          function(request,response)
          {
            console.log('GET' + URL_BASE + 'users/id');
            let indice = request.params.id;
            let instancia = userFile[indice -1];
            console.log(instancia);

            let respuesta = (instancia != undefined) ? instancia : {"mensaje":"Recurso no encontrado."};
            //response.send(200); // codigo error HTTP
            response.send(instancia);

            /*if(instancia != undefined)
              response.send(instancia)
            else {
              response.send({"mensaje":"Recurso no encontrado."})
            }
            response.send(instancia);*/
            //console.log('GET ' + URL_BASE + 'users/id');
            //console.log(request.params.id);
          }
        );
// Peticion GET con Query String
app.get(URL_BASE + 'usersq',
        function(request,response)
        {
            console.log('GET' + URL_BASE + ' con query string ');
            console.log(request.query);
            response.send(respuesta);
        }
      );

//Peticion POST
app.post(URL_BASE + 'users',
        function(req,res)
        {
            totalUsers = userFile.length;
            cuerpo = req.body;
            console.log(cuerpo);

            let newUser = {
                            userID : totalUsers,
                            first_name : req.body.first_name,
                            last_name : req.body.last_name,
                            email : req.body.email,
                            password : req.body.password
                          }

              userFile.push(newUser);
              res.status(201);
              res.send({"mensaje" : "Usuario creado con exito.", "usuario" : newUser});

        }
      );


//Peticion PUT
/*
app.put(URL_BASE + 'users',
        function(req,res)
        {
            totalUsers = userFile.length;
            cuerpo = req.body;
            console.log(cuerpo);

            let newUser = {
                            userID : req.body.userID,
                            first_name : req.body.first_name,
                            last_name : req.body.last_name,
                            email : req.body.email,
                            password : req.body.password
                          }

              userFile.push(newUser);
              res.status(200);
              res.send({"mensaje" : "Usuario actualizado con exito.", "usuario" : newUser});

        }
      );
*/

app.put(URL_BASE + 'users/:id',
          function(req,res)
          {
            var elementosBody = Object.keys(req.body).length;
            if(elementosBody > 0)
            {
              let indice = req.params.id - 1
              if(userFile[indice] != undefined)
              {
                userFile[indice].first_name = req.body.first_name;
                userFile[indice].last_name = req.body.last_name;
                userFile[indice].email = req.body.email;
                userFile[indice].password = req.body.password;
                res.status(200);
                res.send({"mensaje":"Usuario actualizado con exito.", "Array": userFile[indice]});
                console.log(userFile[indice]);
              }
              else
              {
                res.send({"mensaje":"Usuario no se encontro."});
              }
            }
            else
            {
                res.send({"mensaje":"No existe elementos en el Body."});
            }
          }
);

app.post(URL_BASE + 'logout',
          function(req,res)
          {
              var email = req.body.email;
              var password = req.body.password;
              console.log(email);
              console.log(password);
              for(user of userFile)
              {
                if(email == user.email)
                {
                  if(password == user.password)
                  {
                    if(user.logged)
                    {
                      delete user.logged;
                      escribirArchivoUsuario(userFile);
                      res.send({"mensaje":"Logout correcto.",
                                "id": user.userID,
                                "first_name": user.first_name,
                                "last_name": user.last_name});
                    }
                    else {
                      res.send({"mensaje":"Usuario NO logueado"})
                    }
                  }
                  else {
                    res.send({"mensaje":"Logout incorrectos."})

                  }
                }
              }
          }
        );

app.post(URL_BASE + 'login',
          function(req,res)
          {
              var email = req.body.email;
              var password = req.body.password;
              console.log(email);
              console.log(password);
              for(user of userFile)
              {
                if(email == user.email)
                {
                  if( password == user.password)
                  {
                    user.logged = true;
                    escribirArchivoUsuario(userFile);
                    console.log('Login correcto');
                    res.send({"mensaje":"Login correcto.",
                              "userID": user.userID,
                              "first_name": user.first_name,
                              "last_name": user.last_name,
                              "logged": user.logged});
                  }
                  else {
                    res.send({"mensaje":"Login incorrecto."})
                  }
                }
              }
        }
);

app.delete(URL_BASE + 'user',
            function(req,res)
            {

            }
          );

app.get(URL_BASE + 'Logueados',
        function(req,res)
        {
          var logados=new Array();
          for(user of userFile)
          {
            if(user.logged)
            {
              logados.push(user);
            }
          }
          res.send(logados);
        }
      );

function escribirArchivoUsuario(data)
{
  var fs=require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json" ,jsonUserData, "utf8" ,
                function(err){
                  if(err){
                    console.log(err);
                  }
                  else {
                    console.log("Se actualizo archivo 'user.json'.");
                  }
                }
              )
}

app.listen(3000, function() {
    console.log('Api escuchando en puerto 3000...');
});
